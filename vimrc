packloadall
syntax enable
set showmode
set showcmd
set wildmenu
set ruler
runtime ftplugin/man.vim
set autoindent
set expandtab
set nowrap
set hlsearch
set incsearch
set ignorecase
set showmatch
set smartcase
set path=.,../usr/include/**,/usr/share/**
set nu
set spelllang=en_us
set tabstop=8
set shiftwidth=4
set shiftround
set autoindent
