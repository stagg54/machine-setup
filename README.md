# Machine Setup

Scripts for setting up new machines using Choco.

Open Powershell as admin.

Paste and run

```
Invoke-WebRequest "https://gitlab.com/stagg54/machine-setup/-/raw/main/Generic_Dev_Machine.ps1" | Select-Object -ExpandProperty Content | Out-File "Generic_Dev_Machine.ps1"; ./Generic_Dev_Machine.ps1;
rm ./Generic_Dev_Machine.ps1
```

