[CmdletBinding(PositionalBinding=$false)]
Param(
	[ValidateSet("2019", "2020", "2021")]
	[string] $year = "2022"#,

#[switch] $x64
)

./AddFeeds.ps1 -year $year
$bitness = "" #If ($x64) { "" } Else { "-x86" }
nipkg.exe install --accept-eulas -y `
ni-labview-$year-core$bitness-en `
ni-certificates `
ni-labview-$year-vi-analyzer-toolkit$bitness `
#ni-labview-${env:year}-aspt-toolkit${env:bitness} `
#ni-labview-${env:year}-cdsim-module${env:bitness} `
#"ni-labview-${env:year}-mathscript-module$(If (${env:bitness} -eq '-x86') { '-x86-en' } Else { '' })" `
#ni-sv-toolkit-${env:year}${env:bitness} `
ni-visa-labview-$year-support$bitness ; `
ni-vipm-$year
if( $LASTEXITCODE.Equals(-125071) ) { `
	# Needs restart 
	Write-Output 'Exiting successfully' `
	exit 0 `
} else { `
	exit $LASTEXITCODE `
}

