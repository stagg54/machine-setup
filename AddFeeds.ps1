# Originally copied from here: https://github.com/oist/LabVIEW_Dockerfiles/blob/main/AddFeeds.ps1
# thanks Christian!
#
# This script adds feeds to nipkg for a specific build set
# Defaults focus on LabVIEW 2021, 32-bit.

[CmdletBinding(PositionalBinding=$false)]
Param(
	[ValidateSet("2019", "2020", "2021")]
	[string] $year = "2022"#,

#	[switch] $x64
)

$bitness = ""#If ($x64) { "" } Else { "-x86" }
$lvVersionLookup = @{"2019"="19.1"; "2020"="20.1"; "2021"="21.1";}
$lvVersion = $lvVersionLookup.$year
$moduleVersionNumLookup = @{"2019"="19.0"; "2020"="20.0"; "2021"="21.0";}
$moduleVersionNum = $moduleVersionNumLookup.$year
$svVersionLookup = @{"2019"="19.0"; "2020"="20.0"; "2021"="21.0"}
$svVersion = $svVersionNumLookup.$year
$crioVersionLookup = @{"2019"="19.5";"2020"="20.5";"2021"="21.0"}
$crioVersion = $crioVersionLookup.$year
$visaVersionLookup = @{"2019"="19.5";"2020"="20.5";"2021"="21.0"}
$visaVersion = $visaVersionLookup.$year
$daqmxVersionLookup = @{"2019"="19.5";"2020"="20.5";"2021"="21.0"}
$daqmxVersion = $daqmxVersionLookup.$year
$vipmVersionLookup = @{"2019"="19.0";"2020"="20.0";"2021"="21.0"}
$vipmVersion = $vipmVersionLookup.$year
$pkg_root = "https://download.ni.com/support/nipkg/products"

# Feeds are added for 64-bit rt/fpga, but won't be installed by the Dockerfile
$lv_modules =  "vi-analyzer-toolkit"
# The "core" modules don't include core in the URL
nipkg.exe feed-add --name="""ni-labview-$year-core$bitness-en-$year-released""" --system $("$pkg_root/ni-l/ni-labview-$year$bitness/$lvVersion/released")
nipkg.exe feed-add --name="""ni-labview-$year-core$bitness-en-$year-released-critical""" --system $("$pkg_root/ni-l/ni-labview-$year$bitness/$lvVersion/released-critical")
#VIPM
nipkg.exe feed-add --name="""ni-vipm-$year-released""" --system $("$pkg_root/ni-l/ni-vipm/$vipmVersion/released-critical")
nipkg.exe feed-add --name="""ni-vipm-$year-critical""" --system $("$pkg_root/ni-l/ni-vipm/$vipmVersion/critical")


# The other ni-l/ni-labview-... packages have a shared pattern
ForEach($module in $lv_modules)
{
	nipkg.exe feed-add --name="""ni-labview-$year-$module$bitness-en-$year-released""" --system $("$pkg_root/ni-l/ni-labview-$year-$module$bitness/$moduleVersionNum/released")
	nipkg.exe feed-add --name="""ni-labview-$year-$module$bitness-en-$year-released-critical""" --system $("$pkg_root/ni-l/ni-labview-$year-$module$bitness/$moduleVersionNum/released-critical")
}

# Drivers have one package for 32+64 bit, so no need to add differing URLs.
$feeds = @(
	[pscustomobject]@{name = "ni-daqmx-$(${daqmxVersion}.replace(".", "-"))-released"; url = "$($pkg_root)/ni-d/ni-daqmx/$daqmxVersion/released"}
	[pscustomobject]@{name = "ni-compactrio-$(${crioVersion}.replace(".", "-"))-released"; url = "$($pkg_root)/ni-c/ni-compactrio/$crioVersion/released"}
	[pscustomobject]@{name = "ni-visa-$(${visaVersion}.replace(".", "-"))-released"; url = "$($pkg_root)/ni-v/ni-visa/$visaVersion/released"}
	[pscustomobject]@{name = "ni-sv-toolkit-$year$bitness-$year-released"; url = "$($pkg_root)/ni-s/ni-sv-toolkit-$year$bitness/$svVersion/released"}
)

ForEach($pair in $feeds)
{
	nipkg.exe feed-add --name="""$($pair.name)""" --system $($pair.url)
	nipkg.exe feed-add --name="""$($pair.name)-critical""" --system $($pair.url + "-critical")
}
