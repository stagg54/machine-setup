choco feature enable -n=useRememberedArgumentsForUpgrades
choco install microsoft-windows-terminal
choco install sqlitebrowser -y
choco install git -y --params "/NoGuiHereIntegration /WindowsTerminalProfile /DefualtBranchName:main /Editor:VIM"
choco install winscp -y
choco install beyondcompare -y
choco install beyondcompare-integration -y
echo "Setting up LVCompare and LVMerge"
New-Alias -Name gitBash -Value "$Env:ProgramFiles\Git\bin\bash.exe"
gitBash -c "d && git clone https://gitlab.com/sas-blog/LVCompare-Merge-Setup.git && cd LVCompare-Merge-Setup && ./setupLVTools.sh"
choco install python --version 3.12.0 -y
choco install jetbrainstoolbox -y
choco install pycharm -y
choco install gitkraken  -y --ignore-checksums
choco install snagit  -y --ignore-checksums
choco install camtasia  -y --ignore-checksums
choco install vim  -y /NODefaultVimrc /NoDesktopShortcuts
choco install rust-ms  -y
choco install vscode --params "/NoDesktopIcon"
choco install tldr
choco install bitwarden
choco install office-tool
echo "Setting up vimrc and bashrc files"
curl -o .vimrc https://gitlab.com/stagg54/machine-setup/-/raw/main/vimrc
curl -o .bashrc https://gitlab.com/stagg54/machine-setup/-/raw/main/winbashrc
echo "Creating SSH Key - will need added to GitHub/GitLab, etc."
cd ~
ssh-keygen -t ed25519 -q -f .ssh/id_ed25519
echo "For MS Office load MS Office Tool. Follow instructions. For product type choose MS365 Apps for Business. Login using sam@automatedenver.onmicrosoft.com and PW from bitwarden."

